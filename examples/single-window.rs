use std::time::Duration;
use std::cell::RefCell;
use std::rc::Rc;

use pushrod::engine::{Engine, EventHandler};
use pushrod::caches::WidgetCache;
use pushrod::event::Event::Pushrod;
use pushrod::event::{Event, PushrodEvent};
use pushrod::primitives::init_application;
use pushrod::properties::{
    PROPERTY_BORDER_COLOR, PROPERTY_BORDER_WIDTH, PROPERTY_FONT_NAME, PROPERTY_FONT_SIZE,
    PROPERTY_FONT_STYLE, PROPERTY_MAIN_COLOR, PROPERTY_TEXT, PROPERTY_TEXT_JUSTIFICATION,
    PROPERTY_TOGGLED, TEXT_JUSTIFY_CENTER, TEXT_JUSTIFY_LEFT, TEXT_JUSTIFY_RIGHT,
};
use pushrod_widgets::button_widget::ButtonWidget;
use pushrod_widgets::checkbox_widget::CheckBoxWidget;
use pushrod_widgets::toggle_button_widget::ToggleButtonWidget;
use pushrod::widget::Widget;
use sdl2::pixels::Color;

use pushrod::window::Window;

#[derive(Default)]
pub struct PushrodExample {}

impl EventHandler for PushrodExample {
    fn handle_event(&mut self, event: Event, _cache: &mut WidgetCache) {
        match event {
            Pushrod(pushrod_event) => match pushrod_event {
                PushrodEvent::DrawFrame { .. } => {}
                x => eprintln!("Pushrod unhandled event: {:?}", x),
            },
            Event::SDL2(x) => {
                eprintln!("SDL2 unhandled event: {:?}", x);
            }
        }
    }

    fn build_layout(&mut self, cache: &mut WidgetCache) {
        let mut button1 = ButtonWidget::default();

        button1
            .properties()
            .set_origin(20, 20)
            .set_bounds(360, 60)
            .set_color(PROPERTY_MAIN_COLOR, Color::WHITE)
            .set(
                PROPERTY_FONT_NAME,
                String::from("assets/OpenSans-Regular.ttf"),
            )
            .set_value(PROPERTY_FONT_SIZE, 18)
            .set_value(PROPERTY_FONT_STYLE, sdl2::ttf::FontStyle::NORMAL.bits())
            .set_value(PROPERTY_TEXT_JUSTIFICATION, TEXT_JUSTIFY_CENTER)
            .set_value(PROPERTY_BORDER_WIDTH, 2)
            .set_color(PROPERTY_BORDER_COLOR, Color::BLACK)
            .set(PROPERTY_TEXT, String::from("Click Me!"));

        cache.add(Box::new(button1), String::from("button1"), 0);
    }
}

fn main() {
    let (sdl_context, _, sdl_window, ttf_context) = init_application("pushrod example", 600, 800);
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut canvas = sdl_window
        .into_canvas()
        .target_texture()
        .accelerated()
        .build()
        .unwrap();

    let mut window1 = Window::new(400, 260, 200, 400, Box::new(PushrodExample::default()), &mut canvas,
        Rc::clone(&ttf_context));
    let mut window2 = Window::new(400, 260, 0, 0, Box::new(PushrodExample::default()), &mut canvas,
        Rc::clone(&ttf_context));

    window1.build_layout();
    window2.build_layout();

    'mainloop: loop
    {
        for event in event_pump.poll_iter()
        {
            if window1.handle_event(event.clone())
            {
                continue;
            }
            if window2.handle_event(event.clone())
            {
                continue;
            }

            match event
            {
                sdl2::event::Event::Quit {..} =>
                {
                    break 'mainloop;
                }
                _ => {}
            }
        }

        canvas.set_draw_color(Color::RGB(0, 0, 200));
        canvas.clear();
        window1.tick(&mut canvas);
        window2.tick(&mut canvas);
        canvas.present();

        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 30));
    }
}
