// Window struct for Pushrod
// Based on Pushrod Engine
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::boxed::Box;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use std::rc::Rc;
use std::cell::RefCell;

use sdl2::ttf::Sdl2TtfContext;

use crate::engine::EventHandler;
use crate::caches::WidgetCache;
use crate::event::{ Event, PushrodEvent };
use crate::properties::PROPERTY_NEEDS_LAYOUT;
use crate::event::PushrodEvent::{DrawFrame, WidgetRadioSelected};
use crate::widget::Widget;

#[derive(Default)]
pub struct WidgetAddList {
    add_list: Vec<Box<dyn Widget>>,
    parent_id: u32,
}

pub struct Window
{
    width: u32,
    height: u32,
    x: u32,
    y: u32,
    handler: Box<dyn EventHandler>,
    cache: WidgetCache,

    texture_creator: Box<sdl2::render::TextureCreator<sdl2::video::WindowContext>>,
    texture: Box<sdl2::render::Texture>,

    mouse_in_our_borders: bool,
    click_is_ours: bool,
    current_widget_id: u32,
}

impl Window
{
    pub fn new(width: u32, height: u32, x: u32, y: u32,
        handler: Box<dyn EventHandler>,
        canvas: &mut sdl2::render::Canvas<sdl2::video::Window>,
        ttf_context: Rc<RefCell<Sdl2TtfContext>>) -> Self
    {
        let texture_creator = Box::new(canvas.texture_creator());
        let texture = Box::new(texture_creator.create_texture_target(None, width, height).unwrap());

        Self
        {
            width, height,
            x, y,
            handler,
            cache: WidgetCache::new(width, height, ttf_context),

            texture_creator,
            texture,

            mouse_in_our_borders: false,
            click_is_ours: false,
            current_widget_id: 0
        }
    }

    /// Retrieves the `WidgetCache`.
    pub fn get_cache(&mut self) -> &mut WidgetCache {
	&mut self.cache
    }

    /// Broadcasts a generated `PushrodEvent` to the current `Widget`, capturing the response, and
    /// forwarding it on to the application if an event was returned.
    fn send_event_to_widget(&mut self, widget_id: u32, event: PushrodEvent) {
	let handled_event = self.cache.get(widget_id).handle_event(event);

	if let Some(x) = handled_event {
	    self.handler
		.handle_event(Event::Pushrod(x), &mut self.cache)
	}
    }

    /// Sends an event to all `Widget`s.
    fn send_event_to_all(&mut self, event: PushrodEvent) {
	let cache_size = self.cache.size();

	for i in 0..cache_size {
	    let handled_event = self.cache.get(i).handle_event(event.clone());

	    if let Some(x) = handled_event {
		self.handler
		    .handle_event(Event::Pushrod(x.clone()), &mut self.cache);

		// RESEND the event ONLY IF the event qualifies as a re-distributable event, as the widget's
		// generated event has already been sent to the handler.  This could potentially cause
		// an infinite loop, so this needs to be used with care.
		//
		// Clippy is complaining about this being only one event.  This will eventually become
		// multiple events that are handled by a "rebroadcast" flag soon.
		match x {
		    WidgetRadioSelected { .. } => self.send_event_to_all(x.clone()),
		    _ => {}
		}
	    }
	}
    }

    /// This function handles the `MouseMotion` event, converting it into an `Event` that can be
    /// used by `Pushrod`.  The X and Y coordinates are translated into relative offsets based on the
    /// position of the `Widget`.  This way, the X and Y coordinates can be based on drawing
    /// functions inside the `Widget` if necessary.
    fn handle_mouse_move(&mut self, x: u32, y: u32) {
	let cur_widget_id = self.current_widget_id;

        if x < self.x || x > self.x + self.width ||
            y < self.y || y > self.y + self.height
        {
            self.mouse_in_our_borders = false;

            eprintln!("move ({}, {}) does not belong to us ({} - {}, {} - {})",
                    x, y, self.x, self.y, self.x+self.width, self.y+self.height);
            return;
        }

        self.mouse_in_our_borders = true;

        let x = x - self.x;
        let y = y - self.y;

	self.current_widget_id = self.cache.id_at_point(x as u32, y as u32);

	if cur_widget_id != self.current_widget_id {
	    let exited_event = PushrodEvent::WidgetMouseExited {
		widget_id: cur_widget_id,
	    };

	    self.handler
		.handle_event(Event::Pushrod(exited_event.clone()), &mut self.cache);

	    self.send_event_to_widget(cur_widget_id, exited_event);

	    let entered_event = PushrodEvent::WidgetMouseEntered {
		widget_id: self.current_widget_id,
	    };

	    self.handler
		.handle_event(Event::Pushrod(entered_event.clone()), &mut self.cache);

	    self.send_event_to_widget(self.current_widget_id, entered_event);
	}

	let points = self
	    .cache
	    .get(self.current_widget_id)
	    .properties()
	    .get_origin();
	let event = PushrodEvent::MouseMoved {
	    widget_id: self.current_widget_id,
	    x: (x - points.0),
	    y: (y - points.1),
	};

	self.handler
	    .handle_event(Event::Pushrod(event.clone()), &mut self.cache);

	self.send_event_to_widget(self.current_widget_id, event);
    }

    /// Handles a `MouseButton` event, which indicates that a mouse button has been pressed or released.
    fn handle_mouse_button(&mut self, mouse_button: u32, state: bool) {
	let event = PushrodEvent::MouseButton {
	    widget_id: self.current_widget_id,
	    button: mouse_button,
	    state,
	};

	self.handler
	    .handle_event(Event::Pushrod(event.clone()), &mut self.cache);

	if !state {
	    self.send_event_to_all(event);
	} else {
	    self.send_event_to_widget(self.current_widget_id, event);
	}
    }

    /// Handles a draw frame event.  This is a timer tick event that can be used by an application
    /// to refresh positions, redraw 3D objects, etc.  It provides a display tick so that the
    /// application can refresh at a rate of 60 frames/sec.
    #[inline]
    fn handle_draw_frame(&mut self, timestamp: u128) {
	let event = DrawFrame { timestamp };

	self.handler
	    .handle_event(Event::Pushrod(event.clone()), &mut self.cache);

	let widget_count = self.cache.size();

	for i in 0..widget_count {
	    let handled_event = self.cache.get(i).handle_event(event.clone());

	    if let Some(x) = handled_event {
		self.handler
		    .handle_event(Event::Pushrod(x), &mut self.cache)
	    }
	}
    }

    /// This function handles the building of additional `Widget`s to the `WidgetCache` if a newly
    /// added `Widget` (or one that has been interacted with) needs to have additional `Widget`s added
    /// to the `WidgetCache`.
    fn handle_build_layout(&mut self) {
	let num_widgets = self.cache.size();
	let mut add_list: Vec<WidgetAddList> = Vec::new();

	// First, build the list of Widgets that are required for each Widget that needs a layout.
	// Any Widgets that need to be built are stored in the add_list, and are processed after
	// processing the list that needs to be built.
	for i in 0..num_widgets {
	    let mut widget = self.cache.get(i);

	    if widget.properties().get_bool(PROPERTY_NEEDS_LAYOUT) {
		let widget_list = widget.build_layout();
		let mut add_entries = WidgetAddList::default();

		add_entries.add_list = widget_list;
		add_entries.parent_id = i;
		add_list.push(add_entries);

		widget.properties().delete(PROPERTY_NEEDS_LAYOUT);
	    }
	}

	// Skip over the next logic if the list is empty.
	if add_list.is_empty() {
	    return;
	}

	// Walk the tree of widgets to add, and add them directly to the cache here.
	for addable in add_list {
	    let widget_list = addable.add_list;
	    let parent_id = addable.parent_id;
	    let resulting_ids = self.cache.add_vec(widget_list, parent_id);

	    eprintln!("IDs: {:?}", resulting_ids);

	    self.cache
		.get_mut(parent_id)
		.constructed_layout_ids(resulting_ids);
	}
    }

    pub fn build_layout(&mut self)
    {
	self.handler.build_layout(&mut self.cache);
    }

    /// handle_event() -> bool if the event belongs to the window and was
    ///     handled
    pub fn handle_event(&mut self, event: sdl2::event::Event) -> bool
    {
        match event
        {
            sdl2::event::Event::MouseMotion { x, y, .. } => {
                self.handle_mouse_move(x as u32, y as u32);

                false // Mouse motions should be delivered to everyone?:)
            }

            sdl2::event::Event::MouseButtonDown { mouse_btn, .. } => {
                // if the mouse cursor is within our borders
                if self.mouse_in_our_borders
                {
                    self.click_is_ours = true;
                    self.handle_mouse_button(mouse_btn as u32, true);

                    true
                } else {
                    false
                }
            }

            sdl2::event::Event::MouseButtonUp { mouse_btn, .. } => {
                // if the mouse click was initiated within our borders
                if self.click_is_ours
                {
                    self.click_is_ours = false;
                    self.handle_mouse_button(mouse_btn as u32, false);

                    true
                } else {
                    false
                }
            }

            _ => { false }
        }
    }

    pub fn tick(&mut self, canvas: &mut sdl2::render::Canvas<sdl2::video::Window>)
    {
        let viewport = sdl2::rect::Rect::new(
                self.x as i32,
                self.y as i32,
                self.width,
                self.height
            );

        self.handle_draw_frame(
            SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis(),
        );

        // Any Widgets that need a layout can be handled here.
        if self.cache.needs_layout()
        {
            eprintln!("Needs layout");
            self.handle_build_layout();
        }

        let texture = &mut self.texture;

        // Draw the screen if any widgets have been invalidated or added to the display list
        if self.cache.invalidated()
        {
            // Draw after events are processed.
            let cache = &mut self.cache;
            canvas.with_texture_canvas(texture, |canvas_arg| cache.refresh(canvas_arg));
        }

        canvas.copy(texture, None, viewport);
    }
}
