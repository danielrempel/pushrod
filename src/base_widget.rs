// Pushrod Widgets
// Base Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;

use crate::caches::TextureCache;
use crate::primitives::draw_base;
use crate::properties::WidgetProperties;
use crate::texture_store::TextureStore;
use crate::widget::Widget;
use crate::widget_default_impl;

/// Base Widget.
#[derive(Default)]
pub struct BaseWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
}

/// Implementation for drawing a `BaseWidget`, with the `Widget` trait objects applied.
impl Widget for BaseWidget {
    widget_default_impl!("BaseWidget");

    /// This is a `BaseWidget` that is used as a standard blank `Widget`.
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_BORDER_WIDTH: the width of the border to draw
    /// - PROPERTY_BORDER_COLOR: the `Color` of the border.
    fn draw(&mut self, c: &mut Canvas<Window>, _t: &mut TextureCache) -> Option<&Texture> {
        // ONLY update the texture if the `BaseWidget` shows that it's been invalidated.
        if self.invalidated() {
            let bounds = self.properties.get_bounds();

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let cloned_properties = self.properties.clone();

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, None)
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }
}
